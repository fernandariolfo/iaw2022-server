import { UserRepository } from '@loopback/authentication-jwt';
import { CronJob, cronJob } from '@loopback/cron';
import { repository } from '@loopback/repository';
import { Archivo } from '../models/archivo.model';
import { ArchivoRepository, UsuarioRepository } from '../repositories';
import moment from 'moment';

@cronJob()
export class VencimientoJob extends CronJob {
  constructor(@repository(ArchivoRepository) public archivoRepository: ArchivoRepository,
    @repository(UserRepository) public usuarioRepository: UserRepository) {
    super({
      name: 'job-B', onTick: async () => {
        let archivosArray: Archivo[] = await archivoRepository.find({ where: { vencimiento: false || undefined } });
        //console.log(archivosArray);
        await Promise.all(
          archivosArray.map(async (archivo: Archivo) => {
            //if (archivo.usuarioId != undefined) {
            let usuarioArchivo: any = await usuarioRepository.findOne({ where: { id: archivo.usuarioId } })
            if (usuarioArchivo != null) {
              //console.log(usuarioArchivo)
              console.log(archivo.fecha)
              // let fechaToday = new Date();
              // let fechaArchivo = new Date(archivo.fecha)
              // console.log(fechaToday.toLocaleDateString())
              // console.log(fechaArchivo.toLocaleDateString())
              let fechaToday = moment();
              let fechaArchivo = moment(archivo.fecha)
              //console.log(fechaToday)
              //console.log(fechaArchivo)
              let esValido = fechaToday.diff(fechaArchivo, 'days')
              //console.log(esValido);
              if (usuarioArchivo.tipo == 'free') {
                if (esValido > 7) {
                  archivo.vencimiento = true
                }
              } else {
                if (usuarioArchivo.tipo == 'pro') {
                  if (esValido > 30) {
                    archivo.vencimiento = true
                  }
                }
              }
              await archivoRepository.save(archivo)
            }
            console.log(archivo)
            return archivo
          })
        )
        // for ( let i = 0; i < archivosArray.length; i++ ) {
        //   //console.log(archivosArray[i] );
        //   archivoUsuario = usuarioRepository.findOne({ where: { id: archivosArray[i].usuarioId } })
        //   console.log(archivoUsuario);
        // }
      },
      cronTime: '*/10 * * * * *',
      start: true,
    });
  }
}
