import {Entity, model, property} from '@loopback/repository';

@model()
export class Archivo extends Entity {
  @property({
    type: 'string',
    required: true,
  })
  nombre: string;

  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'number',
    required: true,
  })
  tamanio: number;

  @property({
    type: 'string',
  })
  usuarioId?: string;

  @property({
    type: 'number',
    required: true,
  })
  descargas: number;

  @property({
    type: 'string',
    required: true,
  })
  version: string;

  @property({
    type: 'date',
    required: true,
  })
  fecha: Date;

  @property({
    type: 'string'
  })
  clave?: string;

  @property({
    type: 'date'
  })
  caducidad?: Date;

  @property({
    type: 'string',
    default: 'home'
  })
  directorio?: string;

  @property({
    type: 'number'
  })
  size?: number;

  @property({
    type: 'string'
  })
  restriccion?: string;

  @property({
    type: 'string'
  })
  url?: string;

  @property({
    type: 'boolean',
    default: false,
  })
  vencimiento?: boolean;

  constructor(data?: Partial<Archivo>) {
    super(data);
  }
}

export interface ArchivoRelations {
  // describe navigational properties here
}

export type ArchivoWithRelations = Archivo & ArchivoRelations;
