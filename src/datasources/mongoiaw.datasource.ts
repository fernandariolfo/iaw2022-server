import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';

const config = {
  name: 'mongoiaw',
  connector: 'mongodb',
  url: 'mongodb+srv://mfernandariolfo:melo2208.@iaw2022.mga6jiw.mongodb.net/test?',
  host: '',
  port: 0,
  user: '',
  password: '',
  database: '',
  useNewUrlParser: true
};

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class MongoiawDataSource extends juggler.DataSource
  implements LifeCycleObserver {
  static dataSourceName = 'mongoiaw';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.mongoiaw', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
