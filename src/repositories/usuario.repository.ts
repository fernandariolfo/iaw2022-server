import {Getter, inject} from '@loopback/core';
import {DefaultCrudRepository, HasManyRepositoryFactory, repository} from '@loopback/repository';
import {MongoiawDataSource} from '../datasources/mongoiaw.datasource';
import {Archivo, Usuario, UsuarioRelations} from '../models';
import {ArchivoRepository} from './archivo.repository';

export class UsuarioRepository extends DefaultCrudRepository<
  Usuario,
  typeof Usuario.prototype.id,
  UsuarioRelations
> {

  public readonly archivos: HasManyRepositoryFactory<Archivo, typeof Usuario.prototype.id>;

  constructor(
    @inject('datasources.mongoiaw') dataSource: MongoiawDataSource, @repository.getter('ArchivoRepository') protected archivoRepositoryGetter: Getter<ArchivoRepository>,
  ) {
    super(Usuario, dataSource);
    this.archivos = this.createHasManyRepositoryFactoryFor('archivos', archivoRepositoryGetter,);
    this.registerInclusionResolver('archivos', this.archivos.inclusionResolver);
  }
}
