import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {MongoiawDataSource} from '../datasources/mongoiaw.datasource';
import {Archivo, ArchivoRelations} from '../models';

export class ArchivoRepository extends DefaultCrudRepository<
  Archivo,
  typeof Archivo.prototype.id,
  ArchivoRelations
> {
  constructor(
    @inject('datasources.mongoiaw') dataSource: MongoiawDataSource,
  ) {
    super(Archivo, dataSource);
  }
}
