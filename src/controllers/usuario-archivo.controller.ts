import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Usuario,
  Archivo,
} from '../models';
import {UsuarioRepository} from '../repositories';

export class UsuarioArchivoController {
  constructor(
    @repository(UsuarioRepository) protected usuarioRepository: UsuarioRepository,
  ) { }

  @get('/usuarios/{id}/archivos', {
    responses: {
      '200': {
        description: 'Array of Usuario has many Archivo',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Archivo)},
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<Archivo>,
  ): Promise<Archivo[]> {
    return this.usuarioRepository.archivos(id).find(filter);
  }

  @post('/usuarios/{id}/archivos', {
    responses: {
      '200': {
        description: 'Usuario model instance',
        content: {'application/json': {schema: getModelSchemaRef(Archivo)}},
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof Usuario.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Archivo, {
            title: 'NewArchivoInUsuario',
            exclude: ['id'],
            optional: ['usuarioId']
          }),
        },
      },
    }) archivo: Omit<Archivo, 'id'>,
  ): Promise<Archivo> {
    return this.usuarioRepository.archivos(id).create(archivo);
  }

  @patch('/usuarios/{id}/archivos', {
    responses: {
      '200': {
        description: 'Usuario.Archivo PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Archivo, {partial: true}),
        },
      },
    })
    archivo: Partial<Archivo>,
    @param.query.object('where', getWhereSchemaFor(Archivo)) where?: Where<Archivo>,
  ): Promise<Count> {
    return this.usuarioRepository.archivos(id).patch(archivo, where);
  }

  @del('/usuarios/{id}/archivos', {
    responses: {
      '200': {
        description: 'Usuario.Archivo DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(Archivo)) where?: Where<Archivo>,
  ): Promise<Count> {
    return this.usuarioRepository.archivos(id).delete(where);
  }
}
