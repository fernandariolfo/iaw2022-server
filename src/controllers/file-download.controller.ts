import {inject} from '@loopback/core';
import { repository } from '@loopback/repository';
import {
  get,
  HttpErrors,
  oas,
  param,
  Response,
  RestBindings
} from '@loopback/rest';
import fs from 'fs';
import path from 'path';
import {promisify} from 'util';
import {STORAGE_DIRECTORY} from '../keys';
import { ArchivoRepository } from '../repositories';
import { ArchivoController } from './archivo.controller';

const readdir = promisify(fs.readdir);

/**
 * A controller to handle file downloads using multipart/form-data media type
 */
export class FileDownloadController {
  constructor(
  @inject(STORAGE_DIRECTORY) private storageDirectory: string, 
  @repository(ArchivoRepository)
  public archivoRepository : ArchivoRepository

  ) { }
  @get('/files', {
    responses: {
      200: {
        content: {
          // string[]
          'application/json': {
            schema: {
              type: 'array',
              items: {
                type: 'string',
              },
            },
          },
        },
        description: 'A list of files',
      },
    },
  })
  async listFiles() {
    const files = await readdir(this.storageDirectory);
    return files;
  }

  @get('/files/{filename}')
  @oas.response.file()
  downloadFile(
    @param.path.string('filename') fileName: string,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ) {
    const file = this.validateFileName(fileName);
    response.download(file, fileName);
    return response;
  }

  // @get('/files/{id}')
  // @oas.response.file()
  // async downloadFileById(
  //   @param.path.string('id') id: string,
  //   @inject(RestBindings.Http.RESPONSE) response: Response,
  // ) {
  //   console.log(id);
  //   let archivo = await this.archivorepository.findById(id);
  //   console.log(archivo);
  //   const file = this.validateFileName(archivo.nombre);
  //   response.download(file, archivo.nombre);
  //   return response;
  // }

  @get('/files/{filename}/{idfile}')
  @oas.response.file()
  async downloadFileById(
    @param.path.string('filename') fileName: string,
    @param.path.string('idfile') idfile: string,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ) {
    const file = this.validateFileName(fileName);
    let archivo = await this.archivoRepository.findById(idfile);
    await this.archivoRepository.updateById(idfile, {descargas: Number( archivo.descargas ) + 1   });
    response.download(file, fileName);
    return response;
  }

  /**
   * Validate file names to prevent them goes beyond the designated directory
   * @param fileName - File name
   */
  private validateFileName(fileName: string) {
    const resolved = path.resolve(this.storageDirectory, fileName);
    if (resolved.startsWith(this.storageDirectory)) return resolved;
    // The resolved file is outside sandbox
    throw new HttpErrors.BadRequest(`Invalid file name: ${fileName}`);
  }
}
