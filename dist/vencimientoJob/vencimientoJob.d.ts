import { UserRepository } from '@loopback/authentication-jwt';
import { CronJob } from '@loopback/cron';
import { ArchivoRepository } from '../repositories';
export declare class VencimientoJob extends CronJob {
    archivoRepository: ArchivoRepository;
    usuarioRepository: UserRepository;
    constructor(archivoRepository: ArchivoRepository, usuarioRepository: UserRepository);
}
