"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VencimientoJob = void 0;
const tslib_1 = require("tslib");
const authentication_jwt_1 = require("@loopback/authentication-jwt");
const cron_1 = require("@loopback/cron");
const repository_1 = require("@loopback/repository");
const repositories_1 = require("../repositories");
const moment_1 = tslib_1.__importDefault(require("moment"));
let VencimientoJob = class VencimientoJob extends cron_1.CronJob {
    constructor(archivoRepository, usuarioRepository) {
        super({
            name: 'job-B', onTick: async () => {
                let archivosArray = await archivoRepository.find({ where: { vencimiento: false || undefined } });
                //console.log(archivosArray);
                await Promise.all(archivosArray.map(async (archivo) => {
                    //if (archivo.usuarioId != undefined) {
                    let usuarioArchivo = await usuarioRepository.findOne({ where: { id: archivo.usuarioId } });
                    if (usuarioArchivo != null) {
                        //console.log(usuarioArchivo)
                        console.log(archivo.fecha);
                        // let fechaToday = new Date();
                        // let fechaArchivo = new Date(archivo.fecha)
                        // console.log(fechaToday.toLocaleDateString())
                        // console.log(fechaArchivo.toLocaleDateString())
                        let fechaToday = (0, moment_1.default)();
                        let fechaArchivo = (0, moment_1.default)(archivo.fecha);
                        //console.log(fechaToday)
                        //console.log(fechaArchivo)
                        let esValido = fechaToday.diff(fechaArchivo, 'days');
                        //console.log(esValido);
                        if (usuarioArchivo.tipo == 'free') {
                            if (esValido > 7) {
                                archivo.vencimiento = true;
                            }
                        }
                        else {
                            if (usuarioArchivo.tipo == 'pro') {
                                if (esValido > 30) {
                                    archivo.vencimiento = true;
                                }
                            }
                        }
                        await archivoRepository.save(archivo);
                    }
                    console.log(archivo);
                    return archivo;
                }));
                // for ( let i = 0; i < archivosArray.length; i++ ) {
                //   //console.log(archivosArray[i] );
                //   archivoUsuario = usuarioRepository.findOne({ where: { id: archivosArray[i].usuarioId } })
                //   console.log(archivoUsuario);
                // }
            },
            cronTime: '*/10 * * * * *',
            start: true,
        });
        this.archivoRepository = archivoRepository;
        this.usuarioRepository = usuarioRepository;
    }
};
VencimientoJob = tslib_1.__decorate([
    (0, cron_1.cronJob)(),
    tslib_1.__param(0, (0, repository_1.repository)(repositories_1.ArchivoRepository)),
    tslib_1.__param(1, (0, repository_1.repository)(authentication_jwt_1.UserRepository)),
    tslib_1.__metadata("design:paramtypes", [repositories_1.ArchivoRepository,
        authentication_jwt_1.UserRepository])
], VencimientoJob);
exports.VencimientoJob = VencimientoJob;
//# sourceMappingURL=vencimientoJob.js.map