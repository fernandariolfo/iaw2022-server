import { DefaultCrudRepository } from '@loopback/repository';
import { MongoiawDataSource } from '../datasources/mongoiaw.datasource';
import { Archivo, ArchivoRelations } from '../models';
export declare class ArchivoRepository extends DefaultCrudRepository<Archivo, typeof Archivo.prototype.id, ArchivoRelations> {
    constructor(dataSource: MongoiawDataSource);
}
