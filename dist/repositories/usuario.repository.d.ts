import { Getter } from '@loopback/core';
import { DefaultCrudRepository, HasManyRepositoryFactory } from '@loopback/repository';
import { MongoiawDataSource } from '../datasources/mongoiaw.datasource';
import { Archivo, Usuario, UsuarioRelations } from '../models';
import { ArchivoRepository } from './archivo.repository';
export declare class UsuarioRepository extends DefaultCrudRepository<Usuario, typeof Usuario.prototype.id, UsuarioRelations> {
    protected archivoRepositoryGetter: Getter<ArchivoRepository>;
    readonly archivos: HasManyRepositoryFactory<Archivo, typeof Usuario.prototype.id>;
    constructor(dataSource: MongoiawDataSource, archivoRepositoryGetter: Getter<ArchivoRepository>);
}
