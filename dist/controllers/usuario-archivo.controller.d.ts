import { Count, Filter, Where } from '@loopback/repository';
import { Usuario, Archivo } from '../models';
import { UsuarioRepository } from '../repositories';
export declare class UsuarioArchivoController {
    protected usuarioRepository: UsuarioRepository;
    constructor(usuarioRepository: UsuarioRepository);
    find(id: string, filter?: Filter<Archivo>): Promise<Archivo[]>;
    create(id: typeof Usuario.prototype.id, archivo: Omit<Archivo, 'id'>): Promise<Archivo>;
    patch(id: string, archivo: Partial<Archivo>, where?: Where<Archivo>): Promise<Count>;
    delete(id: string, where?: Where<Archivo>): Promise<Count>;
}
