"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArchivoController = void 0;
const tslib_1 = require("tslib");
const authentication_1 = require("@loopback/authentication");
const core_1 = require("@loopback/core");
const security_1 = require("@loopback/security");
const repository_1 = require("@loopback/repository");
const rest_1 = require("@loopback/rest");
const models_1 = require("../models");
const repositories_1 = require("../repositories");
let ArchivoController = class ArchivoController {
    constructor(archivoRepository) {
        this.archivoRepository = archivoRepository;
    }
    async create(currentUserProfile, archivo) {
        archivo.usuarioId = currentUserProfile[security_1.securityId];
        return this.archivoRepository.create(archivo);
    }
    async createSinUsuario(archivo) {
        console.log(archivo);
        return this.archivoRepository.create(archivo);
    }
    async count(where) {
        return this.archivoRepository.count(where);
    }
    async find(currentUserProfile, filter) {
        return this.archivoRepository.find({ where: { usuarioId: currentUserProfile[security_1.securityId] } });
    }
    async updateAll(archivo, where) {
        return this.archivoRepository.updateAll(archivo, where);
    }
    async findById(id, filter) {
        return this.archivoRepository.findById(id, filter);
    }
    async updateById(id, archivo) {
        await this.archivoRepository.updateById(id, archivo);
    }
    async replaceById(id, archivo) {
        await this.archivoRepository.replaceById(id, archivo);
    }
    async deleteById(id) {
        await this.archivoRepository.deleteById(id);
    }
};
tslib_1.__decorate([
    (0, authentication_1.authenticate)('jwt'),
    (0, rest_1.post)('/archivos'),
    (0, rest_1.response)(200, {
        description: 'Archivo model instance',
        content: { 'application/json': { schema: (0, rest_1.getModelSchemaRef)(models_1.Archivo) } },
    }),
    tslib_1.__param(0, (0, core_1.inject)(security_1.SecurityBindings.USER)),
    tslib_1.__param(1, (0, rest_1.requestBody)({
        content: {
            'application/json': {
                schema: (0, rest_1.getModelSchemaRef)(models_1.Archivo, {
                    title: 'NewArchivo',
                    exclude: ['id'],
                }),
            },
        },
    })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], ArchivoController.prototype, "create", null);
tslib_1.__decorate([
    (0, rest_1.post)('/archivosSinUsuario'),
    (0, rest_1.response)(200, {
        description: 'Archivo model instance',
        content: { 'application/json': { schema: (0, rest_1.getModelSchemaRef)(models_1.Archivo) } },
    }),
    tslib_1.__param(0, (0, rest_1.requestBody)({
        content: {
            'application/json': {
                schema: (0, rest_1.getModelSchemaRef)(models_1.Archivo, {
                    title: 'NewArchivo',
                    exclude: ['id'],
                }),
            },
        },
    })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], ArchivoController.prototype, "createSinUsuario", null);
tslib_1.__decorate([
    (0, rest_1.get)('/archivos/count'),
    (0, rest_1.response)(200, {
        description: 'Archivo model count',
        content: { 'application/json': { schema: repository_1.CountSchema } },
    }),
    tslib_1.__param(0, rest_1.param.where(models_1.Archivo)),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], ArchivoController.prototype, "count", null);
tslib_1.__decorate([
    (0, authentication_1.authenticate)('jwt'),
    (0, rest_1.get)('/archivos'),
    (0, rest_1.response)(200, {
        description: 'Array of Archivo model instances',
        content: {
            'application/json': {
                schema: {
                    type: 'array',
                    items: (0, rest_1.getModelSchemaRef)(models_1.Archivo, { includeRelations: true }),
                },
            },
        },
    }),
    tslib_1.__param(0, (0, core_1.inject)(security_1.SecurityBindings.USER)),
    tslib_1.__param(1, rest_1.param.filter(models_1.Archivo)),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], ArchivoController.prototype, "find", null);
tslib_1.__decorate([
    (0, rest_1.patch)('/archivos'),
    (0, rest_1.response)(200, {
        description: 'Archivo PATCH success count',
        content: { 'application/json': { schema: repository_1.CountSchema } },
    }),
    tslib_1.__param(0, (0, rest_1.requestBody)({
        content: {
            'application/json': {
                schema: (0, rest_1.getModelSchemaRef)(models_1.Archivo, { partial: true }),
            },
        },
    })),
    tslib_1.__param(1, rest_1.param.where(models_1.Archivo)),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [models_1.Archivo, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], ArchivoController.prototype, "updateAll", null);
tslib_1.__decorate([
    (0, rest_1.get)('/archivos/{id}'),
    (0, rest_1.response)(200, {
        description: 'Archivo model instance',
        content: {
            'application/json': {
                schema: (0, rest_1.getModelSchemaRef)(models_1.Archivo, { includeRelations: true }),
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.path.string('id')),
    tslib_1.__param(1, rest_1.param.filter(models_1.Archivo, { exclude: 'where' })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], ArchivoController.prototype, "findById", null);
tslib_1.__decorate([
    (0, rest_1.patch)('/archivos/{id}'),
    (0, rest_1.response)(204, {
        description: 'Archivo PATCH success',
    }),
    tslib_1.__param(0, rest_1.param.path.string('id')),
    tslib_1.__param(1, (0, rest_1.requestBody)({
        content: {
            'application/json': {
                schema: (0, rest_1.getModelSchemaRef)(models_1.Archivo, { partial: true }),
            },
        },
    })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String, models_1.Archivo]),
    tslib_1.__metadata("design:returntype", Promise)
], ArchivoController.prototype, "updateById", null);
tslib_1.__decorate([
    (0, rest_1.put)('/archivos/{id}'),
    (0, rest_1.response)(204, {
        description: 'Archivo PUT success',
    }),
    tslib_1.__param(0, rest_1.param.path.string('id')),
    tslib_1.__param(1, (0, rest_1.requestBody)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String, models_1.Archivo]),
    tslib_1.__metadata("design:returntype", Promise)
], ArchivoController.prototype, "replaceById", null);
tslib_1.__decorate([
    (0, rest_1.del)('/archivos/{id}'),
    (0, rest_1.response)(204, {
        description: 'Archivo DELETE success',
    }),
    tslib_1.__param(0, rest_1.param.path.string('id')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String]),
    tslib_1.__metadata("design:returntype", Promise)
], ArchivoController.prototype, "deleteById", null);
ArchivoController = tslib_1.__decorate([
    tslib_1.__param(0, (0, repository_1.repository)(repositories_1.ArchivoRepository)),
    tslib_1.__metadata("design:paramtypes", [repositories_1.ArchivoRepository])
], ArchivoController);
exports.ArchivoController = ArchivoController;
//# sourceMappingURL=archivo.controller.js.map