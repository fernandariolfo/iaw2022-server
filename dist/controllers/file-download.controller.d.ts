/// <reference types="express" />
import { Response } from '@loopback/rest';
import { ArchivoRepository } from '../repositories';
/**
 * A controller to handle file downloads using multipart/form-data media type
 */
export declare class FileDownloadController {
    private storageDirectory;
    archivoRepository: ArchivoRepository;
    constructor(storageDirectory: string, archivoRepository: ArchivoRepository);
    listFiles(): Promise<string[]>;
    downloadFile(fileName: string, response: Response): Response<any, Record<string, any>>;
    downloadFileById(fileName: string, idfile: string, response: Response): Promise<Response<any, Record<string, any>>>;
    /**
     * Validate file names to prevent them goes beyond the designated directory
     * @param fileName - File name
     */
    private validateFileName;
}
