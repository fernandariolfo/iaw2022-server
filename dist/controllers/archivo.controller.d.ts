import { UserProfile } from '@loopback/security';
import { Count, Filter, FilterExcludingWhere, Where } from '@loopback/repository';
import { Archivo } from '../models';
import { ArchivoRepository } from '../repositories';
export declare class ArchivoController {
    archivoRepository: ArchivoRepository;
    constructor(archivoRepository: ArchivoRepository);
    create(currentUserProfile: UserProfile, archivo: Omit<Archivo, 'id'>): Promise<Archivo>;
    createSinUsuario(archivo: Omit<Archivo, 'id'>): Promise<Archivo>;
    count(where?: Where<Archivo>): Promise<Count>;
    find(currentUserProfile: UserProfile, filter?: Filter<Archivo>): Promise<Archivo[]>;
    updateAll(archivo: Archivo, where?: Where<Archivo>): Promise<Count>;
    findById(id: string, filter?: FilterExcludingWhere<Archivo>): Promise<Archivo>;
    updateById(id: string, archivo: Archivo): Promise<void>;
    replaceById(id: string, archivo: Archivo): Promise<void>;
    deleteById(id: string): Promise<void>;
}
