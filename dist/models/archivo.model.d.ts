import { Entity } from '@loopback/repository';
export declare class Archivo extends Entity {
    nombre: string;
    id?: string;
    tamanio: number;
    usuarioId?: string;
    descargas: number;
    version: string;
    fecha: Date;
    clave?: string;
    caducidad?: Date;
    directorio?: string;
    size?: number;
    restriccion?: string;
    url?: string;
    vencimiento?: boolean;
    constructor(data?: Partial<Archivo>);
}
export interface ArchivoRelations {
}
export declare type ArchivoWithRelations = Archivo & ArchivoRelations;
