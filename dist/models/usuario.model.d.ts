import { Entity } from '@loopback/repository';
import { Archivo } from './archivo.model';
export declare class Usuario extends Entity {
    id?: string;
    nombre: string;
    password?: string;
    archivos: Archivo[];
    constructor(data?: Partial<Usuario>);
}
export interface UsuarioRelations {
}
export declare type UsuarioWithRelations = Usuario & UsuarioRelations;
