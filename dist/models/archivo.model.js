"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Archivo = void 0;
const tslib_1 = require("tslib");
const repository_1 = require("@loopback/repository");
let Archivo = class Archivo extends repository_1.Entity {
    constructor(data) {
        super(data);
    }
};
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'string',
        required: true,
    }),
    tslib_1.__metadata("design:type", String)
], Archivo.prototype, "nombre", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'string',
        id: true,
        generated: true,
    }),
    tslib_1.__metadata("design:type", String)
], Archivo.prototype, "id", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'number',
        required: true,
    }),
    tslib_1.__metadata("design:type", Number)
], Archivo.prototype, "tamanio", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'string',
    }),
    tslib_1.__metadata("design:type", String)
], Archivo.prototype, "usuarioId", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'number',
        required: true,
    }),
    tslib_1.__metadata("design:type", Number)
], Archivo.prototype, "descargas", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'string',
        required: true,
    }),
    tslib_1.__metadata("design:type", String)
], Archivo.prototype, "version", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'date',
        required: true,
    }),
    tslib_1.__metadata("design:type", Date)
], Archivo.prototype, "fecha", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'string'
    }),
    tslib_1.__metadata("design:type", String)
], Archivo.prototype, "clave", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'date'
    }),
    tslib_1.__metadata("design:type", Date)
], Archivo.prototype, "caducidad", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'string',
        default: 'home'
    }),
    tslib_1.__metadata("design:type", String)
], Archivo.prototype, "directorio", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'number'
    }),
    tslib_1.__metadata("design:type", Number)
], Archivo.prototype, "size", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'string'
    }),
    tslib_1.__metadata("design:type", String)
], Archivo.prototype, "restriccion", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'string'
    }),
    tslib_1.__metadata("design:type", String)
], Archivo.prototype, "url", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'boolean',
        default: false,
    }),
    tslib_1.__metadata("design:type", Boolean)
], Archivo.prototype, "vencimiento", void 0);
Archivo = tslib_1.__decorate([
    (0, repository_1.model)(),
    tslib_1.__metadata("design:paramtypes", [Object])
], Archivo);
exports.Archivo = Archivo;
//# sourceMappingURL=archivo.model.js.map