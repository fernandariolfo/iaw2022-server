"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MongoiawDataSource = void 0;
const tslib_1 = require("tslib");
const core_1 = require("@loopback/core");
const repository_1 = require("@loopback/repository");
const config = {
    name: 'mongoiaw',
    connector: 'mongodb',
    url: 'mongodb+srv://mfernandariolfo:melo2208.@iaw2022.mga6jiw.mongodb.net/test?',
    host: '',
    port: 0,
    user: '',
    password: '',
    database: '',
    useNewUrlParser: true
};
// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
let MongoiawDataSource = class MongoiawDataSource extends repository_1.juggler.DataSource {
    constructor(dsConfig = config) {
        super(dsConfig);
    }
};
MongoiawDataSource.dataSourceName = 'mongoiaw';
MongoiawDataSource.defaultConfig = config;
MongoiawDataSource = tslib_1.__decorate([
    (0, core_1.lifeCycleObserver)('datasource'),
    tslib_1.__param(0, (0, core_1.inject)('datasources.config.mongoiaw', { optional: true })),
    tslib_1.__metadata("design:paramtypes", [Object])
], MongoiawDataSource);
exports.MongoiawDataSource = MongoiawDataSource;
//# sourceMappingURL=mongoiaw.datasource.js.map