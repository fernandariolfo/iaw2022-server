import { ApplicationConfig, ClaseappApplication } from './application';
export * from './application';
export declare function main(options?: ApplicationConfig): Promise<ClaseappApplication>;
